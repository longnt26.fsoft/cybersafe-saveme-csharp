﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CyberSafe_SaveMe.Common
{
    [Serializable]
    public class UserLogin
    {
        public int UserID { get; set; }
        public string Username { get; set; }
        
    }
}