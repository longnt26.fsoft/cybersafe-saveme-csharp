﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CyberSafe_SaveMe.Models
{
    public class Product
    {
        public int ProductID { get; set; }

        [Display(Name = "Product Name")]
        [Required(ErrorMessage = "Required")]
        public string ProductName { get; set; }

        [Required(ErrorMessage = "Required")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Required")]
        public int Quantity { get; set; }

        [AllowHtml]
        public string Description { get; set; }

    }
}