create database SaveMeDB;

use SaveMeDB;

create table Users(
UserID int primary key IDENTITY(1,1),
Username varchar(255),
Passwd varchar(255),
Name varchar(255),
Phone varchar(255),
Address varchar (255)
);

create table Products(
ProductID int primary key IDENTITY(1,1),
Name varchar(255),
Price decimal(18, 0),
Quantity int,
Description varchar(255),
);

insert into Users (Username, Passwd, Name, Phone, Address)
values ('admin', 'admin', 'admin', '1234567890', 'Fville'),
	   ('LongNT', 'nopass', 'Thanh Long', '0208019', 'Ha Noi');	

select * from Users;

insert into Products(Name, Price, Quantity, Description)
values ('Product1', 10.0, 50, 'Example Des 1'),
	   ('Neo Zeong', 25, 10, 'Third coming of Char'),
	   ('Unicorn', 20, 20, 'Rival of Char');

select * from Products;

